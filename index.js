const admin = require('firebase-admin');
const express = require('express')
const axios = require('axios')
const app = express()

var serviceAccount = {
  "type": "service_account",
  "project_id": "codrcrew",
  "private_key_id": "e1004d359c16c4b7e8a66868460e6868272b7505",
  "private_key": "-----BEGIN PRIVATE KEY-----\nMIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQDa+xPLSyKyzio4\nOu40MMzzfi8DuCHzcTPXr0g/hxR/7Hz6c2rYTp8DJRdRVAJNRP0j2cZyC5GugnUO\ngcgadCzZytVaOY9IYVQSOGer6PeTXRMsWQDaMDBJHSz+Iptj9oMVOu9gzzQI1aIN\n/3vWLAL++xmv03WRPcPpIbgNlBJJCoOYrZa2hHSkmgTBgrJb0GnFc9F3u7dtrvPw\npxobf7zhJJvE8LXmlCYCc6Yo+6l3ic47V/FsVFLEevoUE8D5WhFsyMyPJ0cw6xr6\nVJY2PQDA+avI23WbgxDyspXpnKUs46v96Tu9zhyZnWvXL+H+D8otlnGHrcMV1pZB\nU4X8DXWRAgMBAAECggEAA4uPDXqA7E4kJtblZljn6IVBc5j2i79obHR9AfJI5a1J\ndLCLqUbUkjN2hi2K//8+7eanrM79I8se5AY73BnHZBtBiv1lkXFp8AWTwBe8cllO\nZyhluCKRlY1wXgjnZR3nj19t4q5YFFCJGBrrwVLjFYexKcSNobVTR6HxHd8o0J/g\nRZxJDxhdB8GMdo/Aar5taChTtgeuF4Pp9jIKif4zpkxWPYtUxTm9VCTyqaiQ+NQr\n2EZ+kwFkXcbk+qgZ7bcytVOw+BRlBnRcIAEXjgOEcqT63Fm112+Z8FOmnVuqvzBI\njr+bH/OCMxb5jgeu2y6+IGSWcBbLtNcnb303kT3AhQKBgQD6+jnYyI0aKoasgVOH\n0XmFCg05QAXXyJNXFyceCcbCFUtj/Ckif2SISNw+Ct58UY1BZkt0PeTj6L9oQkAn\nouqN8rvqYqAyoh1PfIWDDsK5Lc2cqY9HTLVEnCYvgk1OfCNeHuiIH4OVpOMfFlS8\nL+1U28vbB3h0qFcCRW2RqlxiEwKBgQDfXO4my5owOUW4QRj5MwhwxpBeOU4X0QUO\n8xM7CRlWXNy1Ccmr4SbL1np/27TKXmlybLuyJI0FYJ21oz1hZ4k6e6SUDcwPxMIS\n0rAOTZUxGL0wVuVZOH/GFQTQUvlqwG1dTjG2YRgQGdfSbBvWsnvmNOgQqtvXFL2F\nWhT9nleeSwKBgEX6OODR4k3WZ6B3mrO221wV1mQNDtQAj9xrjzOX5Dw/NbsWFt4Q\niRZawlXmL+yXzHvcqtMpYsWNIrlOJ5clnKKheHW8tBAKFYQRIyTMm8yIEdlJibKG\nl9AefaMngKiOgkn+8jTqHIU/6TfEwTGLZ3AOrMNQTOyx9zih5RAFYSbTAoGBAI2G\nf2tbdF372/4sWRuEx2NVnSHmo2GZ0d+wCs5MLJ65Bis7gWoDpiZfxox+9GWJPrz+\nwkrUt/nhRuxQ+1JdXOkOcLBSsaqIHAP7NQxLAi3yo+UeGgHXN7aVmNA60sheFq2Z\nHwyVJr8E8OfmEhmOS80IVsZAzDzSamc1Nbmg+5lTAoGALFHjJNh4ZthnA0jkNNGs\nJ6olw7xiEOCufvfw35uZnETiTeJI+uUD91mJdbB0Ao/KpWVnQjxWZfeRlqZQP4ol\nA73VCIyFli13yPS2NU75xm5hUbAXKxCIGGT+Mw5QWWimT/hg3ivmNf68ya2VerSn\ndAXlROmOV6r73ybvGslibgQ=\n-----END PRIVATE KEY-----\n",
  "client_email": "firebase-adminsdk-x2ev9@codrcrew.iam.gserviceaccount.com",
  "client_id": "101941000431403786609",
  "auth_uri": "https://accounts.google.com/o/oauth2/auth",
  "token_uri": "https://oauth2.googleapis.com/token",
  "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
  "client_x509_cert_url": "https://www.googleapis.com/robot/v1/metadata/x509/firebase-adminsdk-x2ev9%40codrcrew.iam.gserviceaccount.com"
};

admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
    databaseURL: "https://codrcrew.firebaseio.com"
});

app.get('/add', (req, res) => {
res.setHeader("Access-Control-Allow-origin","*");
admin.messaging().subscribeToTopic([req.query.token], "push").then(function(res) {res.end();}).catch(function(error) {res.end("error");});
})

app.get('/push', async function(req, res) {
res.setHeader("Access-Control-Allow-origin","*");
var message = {
    "topic" : "push",
    "data": {
      "title": req.query.title || "test",
      "body": req.query.body || "test notification",
      "tag": req.query.tag || "default",
      "notify": req.query.notify=="true"?"true":"false"
  },
    "webpush": {
      "fcm_options": {
        "link": req.query.url || "https://theabbie.github.io"
      }
   }
}
admin.messaging().send(message)
  .then((response) => {
    res.end();
  })
  .catch((error) => {
    res.end("error")
  });
})

app.get('/*', (req, res) => {
res.setHeader("Access-Control-Allow-origin","*");
admin.database().ref("push").set(Object.values(req.query)[0]).then(function() {res.end()})
})

app.listen(process.env.port)
